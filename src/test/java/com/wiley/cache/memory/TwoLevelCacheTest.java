package com.wiley.cache.memory;

import com.wiley.cache.CacheException;
import com.wiley.cache.memory.storage.Storage;
import com.wiley.cache.memory.replacement.ReplacementAlgorithm;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

class TwoLevelCacheTest {

    private Storage<String,String> level1Storage = mock(Storage.class);
    private Storage<String,String> level2Storage = mock(Storage.class);
    private CacheMemory<String, String> cacheMemory = new TwoLevelCache<>(level1Storage, level2Storage);

    @Test
    void should_return_data_if_level1_contains_it() throws CacheException {
        String key1 = "key1";
        String value1 = "value1";

        when(level1Storage.retrieveItem(key1)).thenReturn(value1);
        when(level2Storage.getReplacementAlgorithm()).thenReturn(mock(ReplacementAlgorithm.class));

        String result = cacheMemory.getItem(key1);

        assertThat(result, is(value1));

    }

    @Test
    void should_fetch_data_from_level2_and_set_in_level1_if_level1_does_not_contain_it() throws CacheException {
        String key1 = "key1";
        String value1 = "value1";

        when(level1Storage.retrieveItem(key1)).thenReturn(null);
        when(level2Storage.retrieveItem(key1)).thenReturn(value1);

        String result = cacheMemory.getItem(key1);

        assertThat(result, is(value1));
        verify(level1Storage).storeItem(key1,value1); //should store the data in level1 before return

    }

    @Test
    void should_insert_item_to_both_levels() throws CacheException {
        String key1 = "key1";
        String value1 = "value1";

        cacheMemory.putItem(key1, value1);

        verify(level1Storage).storeItem(key1, value1);
        verify(level2Storage).storeItem(key1, value1);

    }

    @Test
    void should_level2_replacement_algorithm_also_be_aware_of_object_access_even_if_access_happens_on_level1_only() throws CacheException {
        String key1 = "key1";
        String value1 = "value1";

        when(level1Storage.retrieveItem(key1)).thenReturn(value1);

        ReplacementAlgorithm<String> replacementAlgorithm = mock(ReplacementAlgorithm.class);
        when(level2Storage.getReplacementAlgorithm()).thenReturn(replacementAlgorithm);

        String result = cacheMemory.getItem(key1); //getting the item from level 1

        verify(replacementAlgorithm).onCacheItemAccess(key1);

    }
}