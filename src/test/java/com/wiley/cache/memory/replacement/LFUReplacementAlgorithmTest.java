package com.wiley.cache.memory.replacement;

import com.wiley.cache.memory.replacement.lfu.LFUReplacementAlgorithm;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

class LFUReplacementAlgorithmTest {

    ReplacementAlgorithm<String> replacementAlgorithm = new LFUReplacementAlgorithm<>();

    @Test
    void should_return_the_LFU_key_when_cache_has_single_item_case1() {

        replacementAlgorithm.onCacheItemAccess("1");
        String nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
        assertThat(nextKeyToBeEvicted, is("1"));
    }

    @Test
    void should_return_the_LFU_key_when_cache_has_single_item_case2() {

        replacementAlgorithm.onCacheItemAccess("1");
        replacementAlgorithm.onCacheItemAccess("1");
        String nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
        assertThat(nextKeyToBeEvicted, is("1"));
    }

    @Test
    void should_return_LFU_key_when_cache_has_multiple_items() {

        replacementAlgorithm.onCacheItemAccess("1");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("3");
        replacementAlgorithm.onCacheItemAccess("1");
        replacementAlgorithm.onCacheItemAccess("3");


        String nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
        assertThat(nextKeyToBeEvicted, is("2"));
    }

    @Test
    void should_return_the_LFU_key_when_cache_has_multiple_items_but_accessed_in_shuffled_order() {

        replacementAlgorithm.onCacheItemAccess("1");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("3");
        replacementAlgorithm.onCacheItemAccess("1");
        String nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
        assertThat(nextKeyToBeEvicted, is("3"));
    }

    @Test
    void should_return_the_LFU_key_when_cache_has_multiple_items_and_some_items_have_been_removed() {

        replacementAlgorithm.onCacheItemAccess("1");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("3");
        replacementAlgorithm.onCacheItemRemove("1");
        replacementAlgorithm.onCacheItemAccess("5");
        replacementAlgorithm.onCacheItemAccess("3");
        String nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
        assertThat(nextKeyToBeEvicted, is("5"));
    }

    @Test
    void should_return_the_LRU_key_when_multiple_items_has_minimum_frequency_case1() {

        replacementAlgorithm.onCacheItemAccess("1");
        replacementAlgorithm.onCacheItemAccess("1");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("3");
        replacementAlgorithm.onCacheItemAccess("3");
        replacementAlgorithm.onCacheItemAccess("3");
        String nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
        assertThat(nextKeyToBeEvicted, is("1"));
    }

    @Test
    void should_return_the_LRU_key_when_multiple_items_has_minimum_frequency_case2() {

        replacementAlgorithm.onCacheItemAccess("1");
        replacementAlgorithm.onCacheItemAccess("1");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("3");
        replacementAlgorithm.onCacheItemAccess("3");
        replacementAlgorithm.onCacheItemAccess("3");
        replacementAlgorithm.onCacheItemAccess("2");

        String nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
        replacementAlgorithm.onCacheItemRemove(nextKeyToBeEvicted);
        nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
        assertThat(nextKeyToBeEvicted, is("3"));
    }

}