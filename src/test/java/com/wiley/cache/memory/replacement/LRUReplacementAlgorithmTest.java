package com.wiley.cache.memory.replacement;

import com.wiley.cache.memory.replacement.lru.LRUReplacementAlgorithm;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.Matchers.is;

class LRUReplacementAlgorithmTest {

    ReplacementAlgorithm<String> replacementAlgorithm = new LRUReplacementAlgorithm<>();

    @Test
    void should_return_the_LRU_key_when_cache_has_single_item() {

        replacementAlgorithm.onCacheItemAccess("1");
        String nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
        assertThat(nextKeyToBeEvicted, is("1"));
    }

    @Test
    void should_return_LRU_key_when_cache_has_multiple_items() {

        replacementAlgorithm.onCacheItemAccess("1");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("3");
        String nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
        assertThat(nextKeyToBeEvicted, is("1"));
    }

    @Test
    void should_return_the_LRU_key_when_cache_has_multiple_items_but_accessed_in_shuffled_order() {

        replacementAlgorithm.onCacheItemAccess("1");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("3");
        replacementAlgorithm.onCacheItemAccess("1");
        String nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
        assertThat(nextKeyToBeEvicted, is("2"));
    }

    @Test
    void should_return_the_LRU_key_when_cache_has_multiple_items_and_some_items_have_been_removed() {

        replacementAlgorithm.onCacheItemAccess("1");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("2");
        replacementAlgorithm.onCacheItemAccess("3");
        replacementAlgorithm.onCacheItemRemove("1");
        replacementAlgorithm.onCacheItemAccess("5");

        String nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
        assertThat(nextKeyToBeEvicted, is("2"));
    }
}