package com.wiley.cache.memory.replacement;

import com.wiley.cache.memory.replacement.list.DoublyLinkedList;
import com.wiley.cache.memory.replacement.list.ListNode;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.Matchers.is;

class DoublyLinkedListTest {

    DoublyLinkedList<String> linkedList = new DoublyLinkedList<>();

    @Test
    void should_remove_the_head_when_list_has_one_element() {
        ListNode<String> listNode = new ListNode<>("1");
        linkedList.insertToTail(listNode);
        linkedList.removeNode(listNode);
        assertNull(linkedList.getHead());

    }

    @Test
    void should_remove_the_tail_when_the_tail_is_asked_to_remove() {
        ListNode<String> listNode1 = new ListNode<>("1");
        ListNode<String> listNode2 = new ListNode<>("2");
        linkedList.insertToTail(listNode1);
        linkedList.insertToTail(listNode2);
        linkedList.removeNode(listNode2);
        assertThat(linkedList.getTail(), is(listNode1));
        assertThat(linkedList.getHead(), is(listNode1));
    }

    @Test
    void should_remove_a_middle_element_when_a_middle_element_is_given() {
        ListNode<String> listNode1 = new ListNode<>("1");
        ListNode<String> listNode2 = new ListNode<>("2");
        ListNode<String> listNode3 = new ListNode<>("3");
        linkedList.insertToTail(listNode1);
        linkedList.insertToTail(listNode2);
        linkedList.insertToTail(listNode3);
        linkedList.removeNode(listNode2);
        assertThat(linkedList.getTail(), is(listNode3));
        assertThat(linkedList.getHead(), is(listNode1));
        assertThat(linkedList.getHead().getNext() ,is(listNode3)); //first element should be pointed to tail
    }


    @Test
    void should_insert_the_fist_node_as_head() {
        ListNode<String> listNode = new ListNode<>("1");
        linkedList.insertToTail(listNode);
        assertThat(linkedList.getHead() ,is(listNode));
        assertThat(linkedList.getTail() ,is(listNode));
    }

    @Test
    void should_insert_the_second_node_as_tail() {
        ListNode<String> listNode1 = new ListNode<>("1");
        ListNode<String> listNode2 = new ListNode<>("2");
        linkedList.insertToTail(listNode1);
        linkedList.insertToTail(listNode2);
        assertThat(linkedList.getHead() ,is(listNode1));
        assertThat(linkedList.getTail() ,is(listNode2));
    }

    @Test
    void should_insert_the_third_node_as_tail() {
        ListNode<String> listNode1 = new ListNode<>("1");
        ListNode<String> listNode2 = new ListNode<>("2");
        ListNode<String> listNode3 = new ListNode<>("3");
        linkedList.insertToTail(listNode1);
        linkedList.insertToTail(listNode2);
        linkedList.insertToTail(listNode3);
        assertThat(linkedList.getHead() ,is(listNode1));
        assertThat(linkedList.getTail() ,is(listNode3));
        assertThat(linkedList.getHead().getNext() ,is(listNode2));
    }

    @Test
    void should_return_true_for_is_tail_method_when_tail_key_is_passed() {
        ListNode<String> listNode1 = new ListNode<>("1");
        ListNode<String> listNode2 = new ListNode<>("2");
        ListNode<String> listNode3 = new ListNode<>("3");
        linkedList.insertToTail(listNode1);
        linkedList.insertToTail(listNode2);
        linkedList.insertToTail(listNode3);
        boolean isTail = linkedList.isTail("3");
        assertThat(isTail, is(true));
    }

    @Test
    void should_return_false_for_is_tail_method_when_wrong_key_is_passed() {
        ListNode<String> listNode1 = new ListNode<>("1");
        ListNode<String> listNode2 = new ListNode<>("2");
        ListNode<String> listNode3 = new ListNode<>("3");
        linkedList.insertToTail(listNode1);
        linkedList.insertToTail(listNode2);
        linkedList.insertToTail(listNode3);
        boolean isTail = linkedList.isTail("2");
        assertThat(isTail, is(false));
    }
}