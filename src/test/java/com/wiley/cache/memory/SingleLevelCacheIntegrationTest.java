package com.wiley.cache.memory;

import com.wiley.cache.CacheException;
import com.wiley.cache.memory.storage.InMemoryStorage;
import com.wiley.cache.memory.storage.Storage;
import com.wiley.cache.memory.storage.StorageException;
import com.wiley.cache.memory.replacement.ReplacementAlgorithm;
import com.wiley.cache.memory.replacement.lfu.LFUReplacementAlgorithm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

class SingleLevelCacheIntegrationTest {

    private ReplacementAlgorithm<String> replacementAlgorithm;
    private Storage<String, String> storage;

    private SingleLevelCache<String, String> singleLevelCache;

    @BeforeEach
    void setUp() throws StorageException {
        replacementAlgorithm = new LFUReplacementAlgorithm<>();
        storage = new InMemoryStorage<>(replacementAlgorithm, 3, "l1");

        singleLevelCache = new SingleLevelCache<>(storage);
    }

    /**
     * The cache should be able to store an item so that it could be retrieved later.
     * @throws CacheException
     */
    @Test
    void should_store_an_item() throws CacheException {

        String key = "key";
        String value = "value";

        singleLevelCache.putItem(key, value);

        String result = singleLevelCache.getItem(key);
        assertThat(result, is(value));

    }

    @Test
    void should_evict_the_LRU_item_when_cache_is_full() throws CacheException {

        String key1 = "key1";
        String value1 = "value1";

        String key2 = "key2";
        String value2 = "value2";

        String key3 = "key3";
        String value3 = "value3";

        //initially fill the cache with three elements
        singleLevelCache.putItem(key1, value1);
        singleLevelCache.putItem(key2, value2);
        singleLevelCache.putItem(key3, value3);

        //access the items in this order
        singleLevelCache.getItem(key2);
        singleLevelCache.getItem(key1);
        singleLevelCache.getItem(key1);

        //add another item to cache while it is full
        String key4 = "key4";
        String value4 = "value4";
        singleLevelCache.putItem(key4, value4);

        //the least recently used item was key3. Therefore it must no longer be in cache.
        String result = singleLevelCache.getItem(key3);
        assertNull(result);

    }

}