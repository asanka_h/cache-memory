package com.wiley.cache.memory;

import com.wiley.cache.CacheException;
import com.wiley.cache.memory.storage.FileStorage;
import com.wiley.cache.memory.storage.InMemoryStorage;
import com.wiley.cache.memory.storage.Storage;
import com.wiley.cache.memory.storage.StorageException;
import com.wiley.cache.memory.replacement.ReplacementAlgorithm;
import com.wiley.cache.memory.replacement.lru.LRUReplacementAlgorithm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.Matchers.is;

/**
 * Tests the integration with FileStorage, InMemoryStorage and LRU algorithm
 */
class TwoLevelCacheLRUIntegrationTest {

    private ReplacementAlgorithm<String> level1ReplacementAlgo;
    private Storage<String, String> level1Storage;

    private ReplacementAlgorithm<String> level2ReplacementAlgo;
    private Storage<String, String> level2Storage;

    private TwoLevelCache<String, String> twoLevelCache;

    @BeforeEach
    void setUp() throws StorageException {
        level1ReplacementAlgo = new LRUReplacementAlgorithm<>();
        level1Storage = new InMemoryStorage<>(level1ReplacementAlgo, 2, "l1");

        level2ReplacementAlgo = new LRUReplacementAlgorithm<>();
        level2Storage = new FileStorage<>(level2ReplacementAlgo, 3, "l2");

        twoLevelCache = new TwoLevelCache<>(level1Storage, level2Storage);
    }

    /**
     * The cache should be able to store an item so that it could be retrieved later.
     * @throws CacheException
     */
    @Test
    void should_store_an_item() throws CacheException {

        String key = "key";
        String value = "value";

        twoLevelCache.putItem(key, value);

        String result = twoLevelCache.getItem(key);
        assertThat(result, is(value));

    }

    /**
     * The cache should be able to store three items maximum. The level-1 size is 2 and level-2 size is 3.
     * Therefore level 1 will contain the most recently used two items while level 2 will contain all three items
     * @throws CacheException
     */
    @Test
    void should_be_able_to_store_three_items() throws CacheException {

        String key1 = "key1";
        String value1 = "value1";

        String key2 = "key2";
        String value2 = "value2";

        String key3 = "key3";
        String value3 = "value3";

        twoLevelCache.putItem(key1, value1);
        twoLevelCache.putItem(key2, value2);
        twoLevelCache.putItem(key3, value3);

        String result = twoLevelCache.getItem(key1);
        assertThat(result, is(value1));

        result = twoLevelCache.getItem(key2);
        assertThat(result, is(value2));

        result = twoLevelCache.getItem(key3);
        assertThat(result, is(value3));

    }

    @Test
    void should_evict_the_LRU_item_when_cache_is_full() throws CacheException {

        String key1 = "key1";
        String value1 = "value1";

        String key2 = "key2";
        String value2 = "value2";

        String key3 = "key3";
        String value3 = "value3";

        //initially fill the cache with three elements
        twoLevelCache.putItem(key1, value1);
        twoLevelCache.putItem(key2, value2);
        twoLevelCache.putItem(key3, value3);

        //access the items in this order
        twoLevelCache.getItem(key2);
        twoLevelCache.getItem(key1);
        twoLevelCache.getItem(key1);

        //add another item to cache while it is full
        String key4 = "key4";
        String value4 = "value4";
        twoLevelCache.putItem(key4, value4);

        //the least recently used item was key3. Therefore it must no longer be in cache.
        String result = twoLevelCache.getItem(key3);
        assertNull(result);

    }
}