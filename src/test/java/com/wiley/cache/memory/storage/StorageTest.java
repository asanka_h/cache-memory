package com.wiley.cache.memory.storage;

import com.wiley.cache.CacheException;
import com.wiley.cache.memory.replacement.ReplacementAlgorithm;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;
class StorageTest {

    ReplacementAlgorithm<String> replacementAlgorithm = mock(ReplacementAlgorithm.class);

    @Test
    void should_notify_replacement_algorithm_on_object_access() throws CacheException {
        Storage<String,String> storage = new DummyStorageFull<>(replacementAlgorithm);
        String key1 = "key1";
        storage.retrieveItem(key1);
        verify(replacementAlgorithm).onCacheItemAccess(key1);

    }

    @Test
    void should_notify_replacement_algorithm_on_object_insert() throws CacheException {
        Storage<String,String> storage = new DummyStorageEmpty<>(replacementAlgorithm);
        String key1 = "key1";
        String value1 = "value1";

        storage.storeItem(key1, value1);
        verify(replacementAlgorithm).onCacheItemAccess(key1);

    }

    @Test
    void should_notify_replacement_algorithm_on_object_removal() throws CacheException {
        Storage<String,String> storage = new DummyStorageEmpty<>(replacementAlgorithm);
        String key1 = "key1";
        String value1 = "value1";

        storage.removeItem(key1);
        verify(replacementAlgorithm).onCacheItemRemove(key1);

    }


    @Test
    void should_evict_and_free_up_space_if_full() throws CacheException {
        Storage<String,String> storage = new DummyStorageFull<>(replacementAlgorithm);

        String key1 = "key1";
        String value1 = "value1";

        String key2 = "key2";
        String value2 = "value2";

        when(replacementAlgorithm.getNextKeyToBeEvicted()).thenReturn(key2);
        storage.storeItem(key1,value1);

        verify(replacementAlgorithm).onCacheItemAccess(key1);
        verify(replacementAlgorithm).onCacheItemRemove(key2);
        verify(replacementAlgorithm).getNextKeyToBeEvicted();

    }
}