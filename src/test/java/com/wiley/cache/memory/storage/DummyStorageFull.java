package com.wiley.cache.memory.storage;

import com.wiley.cache.memory.replacement.ReplacementAlgorithm;

public class DummyStorageFull<K,V> extends Storage<K,V> {

    public DummyStorageFull(ReplacementAlgorithm<K> replacementAlgorithm) {
        super(replacementAlgorithm);
    }

    @Override
    public boolean exists(K key) {
        return true;
    }

    @Override
    public boolean isFull() {
        return true;
    }

    @Override
    public void clear() throws StorageException {

    }

    @Override
    protected void deleteItem(K key) throws StorageException {

    }

    @Override
    protected V getItem(K key) throws StorageException {

        return (V) key;
    }

    @Override
    protected void saveItem(K key, V item) throws StorageException {

    }
}
