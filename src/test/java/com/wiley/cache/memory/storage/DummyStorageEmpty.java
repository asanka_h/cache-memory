package com.wiley.cache.memory.storage;

import com.wiley.cache.memory.replacement.ReplacementAlgorithm;

public class DummyStorageEmpty<K,V> extends Storage<K,V> {

    public DummyStorageEmpty(ReplacementAlgorithm<K> replacementAlgorithm) {
        super(replacementAlgorithm);
    }

    @Override
    public boolean exists(K key) {
        return false;
    }

    @Override
    public boolean isFull() {
        return false;
    }

    @Override
    public void clear() throws StorageException {

    }

    @Override
    protected void deleteItem(K key) throws StorageException {

    }

    @Override
    protected V getItem(K key) throws StorageException {
        return null;
    }

    @Override
    protected void saveItem(K key, V item) throws StorageException {

    }
}
