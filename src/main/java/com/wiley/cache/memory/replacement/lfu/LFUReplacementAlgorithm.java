package com.wiley.cache.memory.replacement.lfu;

import com.wiley.cache.memory.replacement.ReplacementAlgorithm;
import com.wiley.cache.memory.replacement.list.DoublyLinkedList;
import com.wiley.cache.memory.replacement.list.FrequencyListNode;
import com.wiley.cache.memory.replacement.list.ListNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * The least frequently used item will be given as the next key to be removed
 * There is a counter(frequency) for the items stored in cache.
 * The counter is forgotten if the item is evicted. - The practical implementation
 * If multiple items has the same frequency, the LRU will be selected to evict
 * @param <K>
 */
public class LFUReplacementAlgorithm<K> implements ReplacementAlgorithm<K> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LFUReplacementAlgorithm.class);

    /**
     * keysList is used to store all the keys of cached objects.
     * Head will contain the least frequently used item
     * Tail will contain the most frequently used item
     * to achieve O(1) complexity on cache item insert and removal (No need to sort each time to find the LFU item)
     */
    DoublyLinkedList<K> keysList = new DoublyLinkedList<>();

    /**
     * keysMap is used to keep all the keys of cached objects
     * so that keys can be searched with O(1) complexity - for exists() call
     */
    Map<K, FrequencyListNode<K>> keysMap = new HashMap<>();

    @Override
    public void onCacheItemAccess(K key) {

        boolean alreadyInCache = keysMap.containsKey(key);
        if (alreadyInCache) {
            //the item is in the cache. so increase the access frequency and change the position
            FrequencyListNode<K> listNode = keysMap.get(key);
            keysList.removeNode(listNode); //Remove the item from the list if exists
            listNode.incrementFrequency();
            LOGGER.debug("This key is already in cache. Increasing the frequency by 1. key: [{}] new frequency: [{}]", key, listNode.getFrequency());
            insertNodeToCorrectPosition(listNode);
            return;
        }

        LOGGER.debug("This key is not in the cache. Assigning the frequency as 1. key: [{}]", key);
        //the item is not in the cache. so insert it
        FrequencyListNode<K> listNode = new FrequencyListNode<>(key,1);
        insertNewNode(listNode);
        keysMap.put(key,listNode);

    }

    private void insertNewNode(FrequencyListNode<K> listNode) {
        FrequencyListNode<K> head = (FrequencyListNode<K>) keysList.getHead();
        if (head == null) {
            keysList.insertToHead(listNode);
            return;
        }
        //make this one the head and reposition it
        listNode.setNext(head);
        insertNodeToCorrectPosition(listNode);
    }

    private void insertNodeToCorrectPosition(FrequencyListNode<K> node) {

        FrequencyListNode<K> nextNode = (FrequencyListNode<K>) node.getNext();
        while (nextNode != null && nextNode.getFrequency() <= node.getFrequency()) {
            nextNode = (FrequencyListNode<K>) nextNode.getNext();
        }

        if (nextNode == null) {
            keysList.insertToTail(node);
        } else {
            keysList.insertBefore(nextNode, node);
        }

    }

    @Override
    public void onCacheItemRemove(K key) {
        //remove it from the linked list and items map
        if (keysMap.containsKey(key)) {
            LOGGER.debug("The key [{}] is in the cache. Removing it.", key);
            ListNode<K> node = keysMap.get(key);
            keysList.removeNode(node);
            keysMap.remove(key);
            return;
        }
        LOGGER.debug("The key [{}] is not in the cache. Not removing it.", key);

    }

    @Override
    public K getNextKeyToBeEvicted() {
        if (keysList.getHead() == null) {
            LOGGER.warn("This cache is empty. No key to be evicted!");
            return null;
        }

        return keysList.getHead().getKey();
    }
}
