package com.wiley.cache.memory.replacement.list;

public class FrequencyListNode<K> extends ListNode<K> {

    private int frequency;

    public FrequencyListNode(K key, int frequency) {
        super(key);
        this.frequency = frequency;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public void incrementFrequency() {
        this.frequency++;
    }
}
