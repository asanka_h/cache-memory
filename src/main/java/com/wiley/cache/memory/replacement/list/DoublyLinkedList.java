package com.wiley.cache.memory.replacement.list;

public class DoublyLinkedList<K> {

    private ListNode<K> head;
    private ListNode<K> tail;

    public ListNode<K> getHead() {
        return head;
    }

    public ListNode<K> getTail() {
        return tail;
    }

    /**
     * Remove a given node from the doubly linked list
     * @param node
     */
    public void removeNode(ListNode<K> node) {
        //update the next pointer of previous node
        if(node.getPrev() != null){
            node.getPrev().setNext(node.getNext());

        }else{
            //previous node is null, that means we are going to remove the head element.
            //so update the head to next
            this.head = node.getNext();
        }

        //update the prev pointer of next node
        if(node.getNext() !=null ){
            node.getNext().setPrev(node.getPrev());
        }else{
            //next node is null. That means this is the tail node
            //so update the tail to the previous one
            this.tail = node.getPrev();
        }
    }

    /**
     * Insert a given item to head
     */
    public void insertToTail(ListNode<K> node) {
        if (tail != null) {
            tail.setNext(node);
        }
        node.setPrev(tail);
        node.setNext(null);
        tail = node;

        if (head == null) { //this is the first item
            head = tail;
        }
    }

    /**
     * check whether the given key is in the tail of the list or not
     * @param key
     * @return
     */
    public boolean isTail(K key) {
        if (tail == null) {
            return false;
        }
        return tail.getKey() == key;
    }

    public void insertToHead(FrequencyListNode<K> newNode) {
        if (head == null) {
            head = tail = newNode;
            return;
        }
        head.setPrev(newNode);
        newNode.setPrev(null);
        newNode.setNext(head);
        head = newNode;
    }

    public void insertBefore(FrequencyListNode<K> node, FrequencyListNode<K> nodeToBeInserted) {
        ListNode<K> temp = node.getPrev();
        if (temp == null) {
            //this is the head. So insert the item before the old head and return
            insertToHead(nodeToBeInserted);
            return;
        }
        temp.setNext(nodeToBeInserted);
        nodeToBeInserted.setNext(node);
        nodeToBeInserted.setPrev(temp);
        node.setPrev(nodeToBeInserted);
    }
}
