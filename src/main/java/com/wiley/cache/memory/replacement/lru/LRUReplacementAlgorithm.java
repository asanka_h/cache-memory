package com.wiley.cache.memory.replacement.lru;

import com.wiley.cache.memory.replacement.ReplacementAlgorithm;
import com.wiley.cache.memory.replacement.list.DoublyLinkedList;
import com.wiley.cache.memory.replacement.list.ListNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * An implementation of cache replacement (Eviction strategy) based on LRU policy
 * The data structures inside ReplacementAlgorithm only keeps the keys (Meta data). Not the values. So no worries about the memory usage
 * @param <K>
 */
public class LRUReplacementAlgorithm<K> implements ReplacementAlgorithm<K> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LRUReplacementAlgorithm.class);

    /**
     * keysList is used to store all the keys of cached objects.
     * Head will contain the least recently used item
     * Tail will contain the most recently used item
     * to achieve O(1) complexity on cache item insert and removal (No need to sort each time to find the LRU item)
     */
    DoublyLinkedList<K> keysList = new DoublyLinkedList<>();

    /**
     * keysMap is used to keep all the keys of cached objects
     * so that keys can be searched with O(1) complexity - for exists() call
     */
    Map<K, ListNode<K>> keysMap = new HashMap<>();

    /**
     * This method will be triggered when a cached item is accessed in the cache memory
     * @param key - the key of the accessed cache item
     */
    public void onCacheItemAccess(K key) {
        boolean alreadyInCache = keysMap.containsKey(key);
        boolean isTail = keysList.isTail(key);
        if ( alreadyInCache && isTail) {
            //accessing the last accessed item. so no need to do anything
            LOGGER.debug("This key is already in cache and it is already in the tail. No changes required. key: [{}]", key);
            return;
        }
        if (alreadyInCache) {
            LOGGER.debug("This key is already in cache and it is not in the tail. Moving it to the tail. key: [{}]", key);
            //the item is in the cache but not the last accessed item. so that need to MOVE it to the tail.
            ListNode<K> listNode = keysMap.get(key);
            keysList.removeNode(listNode); //Remove the item from the list if exists
            keysList.insertToTail(listNode); //put the item to the end of the list
            return;
        }

        LOGGER.debug("This key is not in the cache. Adding it to the tail of the list. key: [{}]", key);

        //the item is not in the cache. so insert it
        ListNode<K> listNode = new ListNode<>(key);
        keysList.insertToTail(listNode);
        keysMap.put(key,listNode);

    }

    /**
     * This method will be triggered when a cached item is removed from cache memory
     * @param key - key of the item which was removed from cache
     */
    public void onCacheItemRemove(K key) {
        //remove it from the linked list and items map
        if (keysMap.containsKey(key)) {
            LOGGER.debug("The key [{}] is in the cache. Removing it.", key);
            ListNode<K> node = keysMap.get(key);
            keysList.removeNode(node);
            keysMap.remove(key);
            return;
        }
        LOGGER.debug("The key [{}] is not in the cache. Not removing it.", key);

    }

    @Override
    public K getNextKeyToBeEvicted() {

        if (keysList.getHead() == null) {
            LOGGER.warn("This cache is empty. No key to be evicted!");
            return null;
        }

        return keysList.getHead().getKey();

    }

}
