package com.wiley.cache.memory.replacement;

public interface ReplacementAlgorithm<K> {

    /**
     * This method will be triggered when a cached item is accessed in the cache memory
     * @param key - the key of the accessed cache item
     */
    void onCacheItemAccess(K key);

    /**
     * This method will be triggered when a cached item is removed from cache memory
     * @param key - key of the item which was removed from cache
     */
    void onCacheItemRemove(K key);

    /**
     * Returns the next in line key to be go out from the cache.
     * @return - Key of the next item to be go out from the cache
     */
    K getNextKeyToBeEvicted();

}
