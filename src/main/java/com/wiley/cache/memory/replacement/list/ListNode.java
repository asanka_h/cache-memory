package com.wiley.cache.memory.replacement.list;

/**
 * Keeps the metadata about a cache item
 * @param <K>
 */
public class ListNode<K> {

    private K key;
    private ListNode<K> next;
    private ListNode<K> prev;

    public ListNode(K key) {
        this.key = key;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public ListNode<K> getNext() {
        return next;
    }

    public void setNext(ListNode<K> next) {
        this.next = next;
    }

    public ListNode<K> getPrev() {
        return prev;
    }

    public void setPrev(ListNode<K> prev) {
        this.prev = prev;
    }
}
