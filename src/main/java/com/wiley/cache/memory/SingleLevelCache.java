package com.wiley.cache.memory;

import com.wiley.cache.CacheException;
import com.wiley.cache.memory.storage.Storage;

/**
 * The cache memory implementation which has only one level (one storage)
 * @param <K> - the class of the unique identifier to the memory object
 * @param <V> - the class of the objects stored in memory
 */
public class SingleLevelCache<K,V> implements CacheMemory<K, V> {

    private Storage<K, V> storage;

    public SingleLevelCache(Storage<K, V> storage) {
        this.storage = storage;
    }

    @Override
    public V getItem(K key) throws CacheException {
        return storage.retrieveItem(key);
    }

    @Override
    public void putItem(K key, V value) throws CacheException{
        storage.storeItem(key,value);
    }

    @Override
    public void removeItem(K key) throws CacheException{
        storage.removeItem(key);
    }

    @Override
    public boolean isFull() {
        return storage.isFull();
    }

    @Override
    public void clear() throws CacheException{
        storage.clear();
    }
}
