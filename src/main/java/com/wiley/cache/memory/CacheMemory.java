package com.wiley.cache.memory;

import com.wiley.cache.CacheException;

/**
 * An interface for all cache memory implementations
 * @param <K> - the class of the unique identifier to the memory object
 * @param <V> - the class of the objects stored in memory
 */
public interface CacheMemory<K,V> {

    /**
     * Get item from cache by it's unique key
     * @param key - unique key which represents the data
     * @return - item
     * @throws CacheException
     */
    V getItem(K key) throws CacheException;

    /***
     * Stores a key value pair in cache.
     * @param key - a unique key to identify the data which is stored in cache
     * @param value - the data to be stored in cache
     * @throws CacheException
     */
    void putItem(K key, V value) throws CacheException;

    /**
     * To delete an item from cache.
     * @param key - unique key of the item to be removed from cache.
     * @throws CacheException
     */
    void removeItem(K key) throws CacheException;

    /**
     * Check whether the cache memory is full
     * @return - true if full, false otherwise
     */
    boolean isFull();

    /**
     * Flush all the data in the cache memory
     * @throws CacheException
     */
    void clear() throws CacheException;

}
