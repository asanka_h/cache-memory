package com.wiley.cache.memory.storage;

import com.wiley.cache.memory.replacement.ReplacementAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The template for all any storage.
 * @param <K> - the class of the unique identifier to the memory object
 * @param <V> - the class of the objects stored in memory
 */
public abstract class Storage<K,V> {
    private static final Logger LOGGER = LoggerFactory.getLogger(Storage.class);

    private ReplacementAlgorithm<K> replacementAlgorithm;

    public Storage(ReplacementAlgorithm<K> replacementAlgorithm) {
        this.replacementAlgorithm = replacementAlgorithm;
    }

    /**
     * Get an item from the storage by its key
     * @param key
     * @return
     * @throws StorageException
     */
    public V retrieveItem(K key) throws StorageException {
        V item = this.getItem(key);
        if (item != null) {
            LOGGER.debug("Found the item for key [{}] in storage.", key);
            replacementAlgorithm.onCacheItemAccess(key); //item accessed successfully
        }
        return item;
    }


    /**
     * Store an item in the storage.
     * If the key already there, overriding will happen
     * @param key
     * @param item
     * @throws StorageException
     */
    public void storeItem(K key, V item) throws StorageException {
        if (this.isFull()) {
            LOGGER.debug("The storage is full. Removing an item before storing the key [{}]", key);
            K nextKeyToBeEvicted = replacementAlgorithm.getNextKeyToBeEvicted();
            this.removeItem(nextKeyToBeEvicted);
            LOGGER.debug("Removed the key from storage. key: {}", key);
        }
        this.saveItem(key,item);
        replacementAlgorithm.onCacheItemAccess(key); //item stored successfully
    }

    /**
     * Delete an item from the storage.
     * @param key - the unique identifier for the memory object
     * @throws StorageException
     */
    public void removeItem(K key) throws StorageException{
        this.deleteItem(key);
        replacementAlgorithm.onCacheItemRemove(key); //notify that the item removed from cache
    }

    /**
     * Get the replacement algorithm configured for this storage
     * @return - replacement algorithm
     */
    public ReplacementAlgorithm<K> getReplacementAlgorithm(){
        return this.replacementAlgorithm;
    }

    /**
     * To check whether an item exists in storage or not
     * @param key - the unique identifier for the memory object
     * @return true - if exists, false - otherwise
     */
    public abstract boolean exists(K key);

    /**
     * To check whether the storage is full or not
     * @return - true - if full, false - otherwise
     */
    public abstract boolean isFull();

    /**
     * To flush the storage out
     * @throws StorageException
     */
    public abstract void clear() throws StorageException;


    protected abstract void deleteItem(K key) throws StorageException;

    protected abstract V getItem(K key) throws StorageException;

    protected abstract void saveItem(K key, V item) throws StorageException;

}
