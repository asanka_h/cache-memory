package com.wiley.cache.memory.storage;

import com.wiley.cache.CacheException;

public class StorageException extends CacheException {
    public StorageException(String s, Exception e) {
        super(s,e);
    }

    public StorageException(String s) {
        super(s);
    }
}
