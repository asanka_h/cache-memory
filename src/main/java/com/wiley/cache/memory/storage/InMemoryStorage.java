package com.wiley.cache.memory.storage;

import com.wiley.cache.memory.replacement.ReplacementAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * The storage implementation which keeps the data in the computer Main Memory.
 * @param <K> - the class of the unique identifier to the memory object
 * @param <V> - the class of the objects stored in memory
 */
public class InMemoryStorage<K,V> extends Storage<K,V> {

    private static final Logger LOGGER = LoggerFactory.getLogger(InMemoryStorage.class);
    private final Map<K,V> itemMap;
    private final int maxSize;
    private final String identifier;

    public InMemoryStorage(ReplacementAlgorithm<K> replacementAlgorithm, int maxSize, String identifier) {
        super(replacementAlgorithm);
        itemMap = new HashMap<>(maxSize);
        this.maxSize = maxSize;
        this.identifier = identifier;
        LOGGER.info("In-memory storage created with size [{}] and identifier [{}]", maxSize, identifier);
    }

    @Override
    public V getItem(K key) {
        return itemMap.get(key);
    }

    @Override
    public void saveItem(K key, V item) throws StorageException {

        if (this.isFull()) {
            throw new StorageException("The file storage is full. Please remove an item before insert more. Identifier" + identifier);
        }

        itemMap.put(key, item);
    }

    @Override
    public void deleteItem(K key) {
        itemMap.remove(key);
    }

    @Override
    public boolean isFull() {
        return itemMap.size() >= maxSize;
    }

    @Override
    public void clear() {
        itemMap.clear();
    }

    @Override
    public boolean exists(K key) {
        return itemMap.containsKey(key);
    }
}
