package com.wiley.cache.memory.storage;

import com.wiley.cache.memory.replacement.ReplacementAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Each given object will be stored in a separate file.
 * The storage is not permanent. When application restarts, it will reset.
 * @param <K> - the class of the unique identifier to the memory object
 * @param <V> - the class of the objects stored in memory
 */
public class FileStorage<K,V extends Serializable> extends Storage<K,V> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileStorage.class);
    private final Map<K,String> keyToFileNameMap; // A map is needed to keep the mapping between the file name to the key
    private final String identifier;
    private final int maxSize;
    private final Path path;

    public FileStorage(ReplacementAlgorithm<K> replacementAlgorithm, int maxSize, String identifier) throws StorageException {
        super(replacementAlgorithm);
        this.maxSize = maxSize;
        keyToFileNameMap = new HashMap<>(maxSize);
        this.identifier = identifier;
        try {
            this.path = Files.createTempDirectory(identifier);
        } catch (IOException e) {
            throw new StorageException("Failed to create the temp directory for file storage. Identifier: " + identifier, e);
        }
        LOGGER.info("File storage initialized with MAX_SIZE {} and identifier {} ", this.maxSize, identifier);
    }

    @Override
    public V getItem(K key) throws StorageException {
        if(keyToFileNameMap.containsKey(key)){
            String fileName = keyToFileNameMap.get(key);
            File file = Paths.get(path.toString(),fileName).toFile();

            try(FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)){

                 return (V) objectInputStream.readObject();

            } catch (IOException | ClassNotFoundException e) {
                throw new StorageException("Failed to  read the content from file: " + fileName + " identifier: " + identifier, e);
            }

        }
        LOGGER.debug("Could not find the item in file storage. key: {}", key);
        return null;
    }

    @Override
    public void saveItem(K key, V item) throws StorageException {

        if (this.isFull()) {
            throw new StorageException("The file storage is full. Please remove an item before insert more. Identifier" + identifier);
        }

        //if the key is already there in storage, delete the associated file with it first.
        if (keyToFileNameMap.containsKey(key)) {
            Path fileToBeDeleted = Paths.get(this.path.toString(), keyToFileNameMap.get(key));
            try {
                Files.deleteIfExists(fileToBeDeleted);
            } catch (IOException e) {
                throw new StorageException("Could not delete the file. Filepath: " + fileToBeDeleted.toString(), e);
            }
        }

        //create a file to store the data.
        File fileToStoreData;
        try {
            fileToStoreData = Files.createTempFile(path, "", "").toFile(); //this will return a file with a random and unique name
        } catch (IOException e) {
            throw new StorageException("Could not create the temp file to store data. Key: " + key, e);
        }

        //serialize and store the data in the file.
        try (FileOutputStream fileOutputStream = new FileOutputStream(fileToStoreData);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)){
            objectOutputStream.writeObject(item);
            keyToFileNameMap.put(key, fileToStoreData.getName());
        } catch (IOException e) {
            throw new StorageException("Could not store the item in firestore. Key: " + key, e);
        }

    }

    @Override
    public void deleteItem(K key) throws StorageException {
        if (keyToFileNameMap.containsKey(key)) {
            try {
                Files.deleteIfExists(Paths.get(path.toString(), keyToFileNameMap.get(key)));
            } catch (IOException e) {
                throw new StorageException("Could not delete the key from file storage. Key: " + key + " identifier: " + identifier, e);
            }
            keyToFileNameMap.remove(key);
        }
    }

    @Override
    public boolean isFull() {
        return keyToFileNameMap.size() >= maxSize;
    }

    @Override
    public void clear() throws StorageException {
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            throw new StorageException("Could not clear the file storage identifier: " + identifier, e);
        }
        LOGGER.info("File storage was cleared. identifier: {}", identifier);
    }

    @Override
    public boolean exists(K key) {
        return keyToFileNameMap.containsKey(key);
    }
}
