package com.wiley.cache.memory;

import com.wiley.cache.CacheException;
import com.wiley.cache.memory.storage.Storage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A cache memory implementation to keep the more frequently accessed data closer
 * If the level1 contains the data, return it from there, otherwise get it from level two, set in level1, then return the data
 * @param <K> - the class of the unique identifier to the memory object
 * @param <V> - the class of the objects stored in memory
 */
public class TwoLevelCache<K,V> implements CacheMemory<K, V> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TwoLevelCache.class);
    private final Storage<K,V> level1Storage;
    private final Storage<K,V> level2Storage;

    /**
     * @param level1Storage - the memory for storing more frequently accessed data
     * @param level2Storage - the memory for storing less frequently accessed data
     */
    protected TwoLevelCache(Storage<K,V> level1Storage, Storage<K,V> level2Storage){
        this.level1Storage = level1Storage;
        this.level2Storage = level2Storage;
    }

    @Override
    public V getItem(K key) throws CacheException {

        LOGGER.debug("Getting key from Two level cache. Key: [{}]", key);
        //If the level1 contains the data, return it
        V value = level1Storage.retrieveItem(key);
        if (value != null) {
            LOGGER.debug("Value found in level-1 for key [{}] in Two Level Cache. ", key);
            level2Storage.getReplacementAlgorithm().onCacheItemAccess(key); //since the level2 replacement algorithm also should aware of the object access
            return value;
        }

        LOGGER.debug("Could not find the value for key [{}] in Two Level Cache. ", key);

        // otherwise get it from level two, set in level1, then return the data
        value = level2Storage.retrieveItem(key);

        if (value != null) {
            LOGGER.debug("Value found in level-2 for key [{}] in Two Level Cache. ", key);
            level1Storage.storeItem(key,value); //setting the data in level1 so that it could be fetched directly from level1 next time
        }

        return value;
    }

    @Override
    public void putItem(K key, V value) throws CacheException {
        LOGGER.debug("Storing the value for key [{}] in two level cache", key);
        level1Storage.storeItem(key,value);
        level2Storage.storeItem(key,value);
    }

    @Override
    public void removeItem(K key) throws CacheException {
        LOGGER.debug("Removing the value for key [{}] in two level cache", key);
        level1Storage.removeItem(key);
        level2Storage.removeItem(key);
    }

    @Override
    public boolean isFull() {
        return level1Storage.isFull() && level2Storage.isFull();
    }

    @Override
    public void clear() throws CacheException {
        LOGGER.info("Cleaning up the two level cache");
        level1Storage.clear();
        level2Storage.clear();
        LOGGER.info("Cleaning up was completed for the two level cache");
    }
}
