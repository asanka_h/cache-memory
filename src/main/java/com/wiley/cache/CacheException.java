package com.wiley.cache;

public class CacheException extends Exception{
    public CacheException(String s, Exception e) {
        super(s,e);
    }

    public CacheException(String s) {
        super(s);
    }
}
