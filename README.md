# README #

### What is this repository for? ###

A Java Configurable Cache Library

 - Supported modes
    - Single level
    - Two level
 - Supported Storage Mechanisms
    - In Memory Storage (RAM)
    - File-based storage (Content will be lost when cache restarts)
  - Supported Eviction Algorithms (Replacement)
    - Least Recently Used (LRU)
    - Least Frequently Used (LFU)

### Usage ###

Single Level Cache Memory (Use a different replacement algorithm, and a storage as you wish!):

```
 
 ReplacementAlgorithm<String> replacementAlgo = new new LRUReplacementAlgorithm<>();
 Storage storage = new InMemoryStorage(replacementAlgo, 10, "cache-storage");
 CacheMemory<String,String> cache = new SingleLevelCache<>(storage);
 
 //store items
 cache.putItem("key1", "value1");
 cache.putItem("key2", "value2");

//retrieve items
 cache.getItem("key2");
```

Two Level Cache Memory (Use a different replacement algorithm, and a storage as you wish!):

```
 
 ReplacementAlgorithm<String> r1 = new new LRUReplacementAlgorithm<>();
 Storage level1Storage = new InMemoryStorage(r1, 10, "memory-storage");

 ReplacementAlgorithm<String> r2 = new new LRUReplacementAlgorithm<>();
 Storage level2Storage = new FileStorage(r2, 100, "file-storage");

 CacheMemory<String,String> cache = new TwoLevelCache<>(level1Storage, level2Storage);

 //store items
 cache.putItem("key1", "value1");
 cache.putItem("key2", "value2");

//retrieve items
 cache.getItem("key2");
```